import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;


public class WeatherParser {
    public static void main(String[] args) {
        WeatherParser weatherParser = new WeatherParser();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите полный путь к файлу для парсинга:");
        System.out.println("(Например: D:\\Weather\\data.csv)");
        String path = scanner.nextLine();
        File file = new File(path);
        weatherParser.parser(file);
    }

    private void parser(File file) {
        ArrayList<String> arrayList = new ArrayList<>();
        try {
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            for (int i = 0; i < 10; i++) {
                bufferedReader.readLine();
            }
            String ph;
            while ((ph = bufferedReader.readLine()) != null) {
                arrayList.add(ph);
            }
            fileReader.close();
            bufferedReader.close();

            ArrayList<TheWeatherData> theWeatherDataArrayList = new ArrayList<>();
            for (String s : arrayList) {
                int year = Integer.parseInt(s.substring(0, 4));
                int month = Integer.parseInt(s.substring(4, 6));
                int day = Integer.parseInt(s.substring(6, 8));
                int hour = Integer.parseInt(s.substring(9, 11));
                s = s.substring(14);
                float temperature = Float.parseFloat(s.substring(0, s.indexOf(',')));
                s = s.substring(s.indexOf(',') + 1);
                float humidity = Float.parseFloat(s.substring(0, s.indexOf(',')));
                s = s.substring(s.indexOf(',') + 1);
                float windSpeed = Float.parseFloat(s.substring(0, s.indexOf(',')));
                s = s.substring(s.indexOf(',') + 1);
                float windDirection = Float.parseFloat(s);

                theWeatherDataArrayList.add(new TheWeatherData(year, month, day, hour, temperature,
                        humidity, windSpeed, windDirection));
            }
            float averageTemp = averageTemperature(theWeatherDataArrayList);
            float averageHumidity = averageHumidity(theWeatherDataArrayList);
            float averageWindSpeed = averageWindSpeed(theWeatherDataArrayList);

            int day = 0;
            int hour = 0;
            String theHighestTemp = theHighestTemp(theWeatherDataArrayList, day, hour);

            float theLowestHumidity = theLowestHumidity(theWeatherDataArrayList);
            float theHighestWindSpeed = theHighestWindSpeed(theWeatherDataArrayList);
            String theMostCommonDirection = theMostCommonDirection(theWeatherDataArrayList);

            File file1 = new File("WeatherDataOutput.txt");
            FileWriter fileWriter = new FileWriter(file1);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

            createOutputFile(bufferedWriter, averageTemp, averageHumidity, averageWindSpeed, theHighestTemp,
                    theLowestHumidity, theHighestWindSpeed, theMostCommonDirection);

            bufferedWriter.close();
            fileWriter.close();
            System.out.println("Успешно!");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void createOutputFile(BufferedWriter bufferedWriter, float averageTemp, float averageHumidity, float averageWindSpeed,
                                  String theHighestTemp, float theLowestHumidity, float theHighestWindSpeed, String theMostCommonDirection) throws IOException {
        bufferedWriter.write("Данные наблюдения за погодой за указанный период:\n\n");
        bufferedWriter.write("Средняя температура воздуха составила " + averageTemp + " °C\n");
        bufferedWriter.write("Средняя влажность = " + averageHumidity + " %\n");
        bufferedWriter.write("Средняя скорость ветра в этом районе держалась на отметке " + averageWindSpeed + " км/ч\n");
        bufferedWriter.write(theHighestTemp);
        bufferedWriter.write("Самая низкая замеченная влажность воздуха = " + theLowestHumidity + " %\n");
        bufferedWriter.write("Самая высокая замеченная скорость ветра составила целых " + theHighestWindSpeed + " км/ч\n\n");
        bufferedWriter.write("Чаще всего ветер дул по направлению на " + theMostCommonDirection);
    }

    private String theHighestTemp(ArrayList<TheWeatherData> theWeatherDataArrayList, int day, int hour) {
        float max = theWeatherDataArrayList.get(0).getTemperature();
        for (int i = 1; i < theWeatherDataArrayList.size(); i++) {
            if (theWeatherDataArrayList.get(i).getTemperature() > max) {
                max = theWeatherDataArrayList.get(i).getTemperature();
                day = theWeatherDataArrayList.get(i).getDay();
                hour = theWeatherDataArrayList.get(i).getHour();
            }

        }
        return "Самая высокая температура воздуха (" + max + " °C) была " +
                "замечена " + day + " числа в " + hour + " часов\n\n";
    }

    private String theMostCommonDirection(ArrayList<TheWeatherData> theWeatherDataArrayList) {
        int[] side = new int[4];
        for (TheWeatherData theWeatherData : theWeatherDataArrayList) {
            if (theWeatherData.windDirection >= 45 && theWeatherData.windDirection < 135) {
                side[0]++;
            } else if (theWeatherData.windDirection >= 135 && theWeatherData.windDirection < 225) {
                side[1]++;
            } else if (theWeatherData.windDirection >= 225 && theWeatherData.windDirection < 315) {
                side[2]++;
            } else {
                side[3]++;
            }
        }
        int max = side[0];
        int ph = 0;
        for (int i = 1; i < 3; i++) {
            if (side[i] > max) {
                max = side[i];
                ph = i;
            }
        }
        return switch (ph) {
            case 0 -> "восток";
            case 1 -> "юг";
            case 2 -> "запад";
            case 3 -> "север";
            default -> null;
        };

    }

    private float theHighestWindSpeed(ArrayList<TheWeatherData> theWeatherDataArrayList) {
        float res = theWeatherDataArrayList.get(0).windSpeed;
        for (int i = 1; i < theWeatherDataArrayList.size(); i++) {
            if (theWeatherDataArrayList.get(i).windSpeed > res) {
                res = theWeatherDataArrayList.get(i).windSpeed;
            }
        }
        return res;
    }

    private float theLowestHumidity(ArrayList<TheWeatherData> theWeatherDataArrayList) {
        float res = theWeatherDataArrayList.get(0).humidity;
        for (int i = 1; i < theWeatherDataArrayList.size(); i++) {
            if (theWeatherDataArrayList.get(i).humidity < res) {
                res = theWeatherDataArrayList.get(i).humidity;
            }
        }
        return res;
    }

    private float averageWindSpeed(ArrayList<TheWeatherData> theWeatherDataArrayList) {
        float res = 0;
        for (TheWeatherData theWeatherData : theWeatherDataArrayList) {
            res += theWeatherData.windSpeed;
        }
        return res / theWeatherDataArrayList.size();
    }

    private float averageHumidity(ArrayList<TheWeatherData> theWeatherDataArrayList) {
        float res = 0;
        for (TheWeatherData theWeatherData : theWeatherDataArrayList) {
            res += theWeatherData.humidity;
        }
        return res / theWeatherDataArrayList.size();
    }

    private float averageTemperature(ArrayList<TheWeatherData> theWeatherDataArrayList) {
        float res = 0;
        for (TheWeatherData theWeatherData : theWeatherDataArrayList) {
            res += theWeatherData.temperature;
        }
        return res / theWeatherDataArrayList.size();
    }
}
